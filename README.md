# Lovac Base #

These are core game files, used for Modding reference or Community game balance!
Have Fun!

### How can I help!? ###
If you want to help make balance changes or add new content to the base game,
all you have to do is fork develop make your changes (play test them a bit)
and make a pull request, if the Content managers like what they see they'll merge it in!

### Contribution guidelines ###

* Json Files must be human readable (as much as possible)
* Base game assets can't rely on hardmods.

Special requests will have to be made and carefuly looked over to merge hardmods into the base game.
These will have to be looked at case by case, for now.
